# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django_pandas.io import read_frame
from models import *
from django.core import serializers
from querysets import PriceListQuerySet


def index(request):
    return render_to_response('index.html', {}, context_instance=RequestContext(request))


# /priceList_by_car/carname=(?P<car_name>.+)
def pricelist_by_car_name(request, car_name):
    query = PriceList.objects.pricelist_by_car(car_name)
    data = serializers.serialize('json', list(query), use_natural_foreign_keys=True)
    return HttpResponse(data)


# /priceList_by_company/company=(?P<company>.+)
def pricelist_by_company(request, company):
    query = PriceList.objects.pricelist_by_company(company)
    data = serializers.serialize('json', list(query), use_natural_foreign_keys=True)
    return HttpResponse(data)


# NOT YET ASSIGNED  # NOT YET ACTIVE
def download_pricelist_by_company(request, company):
    query = PriceList.objects.pricelist_by_company(company)
    df = read_frame(query, fieldnames=["car", "company", "price_at_company"])
    df.to_excel("Company_Pricelist.xlsx", engine='openpyxl', index=False)
    return serve_file('Company_Pricelist.xlsx', 'Company_Pricelist.xlsx')


# /cars_of_active_companies
def get_cars_of_active_companies(request):
    query = PriceList.objects.cars_of_active_companies()
    response = serializers.serialize('json', list(query), use_natural_foreign_keys=True)
    return HttpResponse(response)


# /download_cars_of_active_companies # NOT YET ACTIVE
def download_cars_of_active_companies(request):
    active_companies_cars = PriceList.objects.filter(company__is_active=True)
    df = read_frame(active_companies_cars, fieldnames=["car", "price_at_company", "company"])
    df.to_excel("Active_Companies_Cars.xlsx", engine='openpyxl', index=False)
    return serve_file('Active_Companies_Cars.xlsx', 'Active_Companies_Cars.xlsx')


# /minimum_prices
def get_min_price_of_each_company(request):
    query = PriceList.objects.min_of_each_company()
    data = serializers.serialize('json', list(query), use_natural_foreign_keys=True)
    return HttpResponse(data)


# def download_get_min_price_of_each_company(request):
#     df = read_frame(PriceList.objects.min_of_each_company())
#     df.to_excel("Minimum_price_of_each_company.xlsx", engine='openpyxl', index=False)
#     return serve_file('Minimum_price_of_each_company.xlsx', 'Minimum_price_of_each_company.xlsx')


# /active_companies
def get_active_companies(request):
    query = Company.objects.active_companies()
    response = serializers.serialize('json', list(query), fields=('name', 'city', 'phone_number'))
    return HttpResponse(response)


# /yud_companies
def get_companies_contains_yud(request):
    query = Company.objects.companies_with_yud()
    response = serializers.serialize('json', list(query), fields=('name', 'city', 'phone_number'))
    return HttpResponse(response)


# NOT YET ASSIGNED #NOT YET ACTIVE
def download_get_companies_with_yud(request):
    # yud_companies = Company.objects.filter(is_active=True)
    # yud_companies = yud_companies.filter(name__contains="י")
    query = Company.objects.companies_with_yud()
    df = read_frame(query, fieldnames=["name", "city", "phone_number", "is_active"])
    df.to_excel("Companies_with_yud.xlsx", engine='openpyxl', index=False)
    return serve_file('Companies_with_yud.xlsx', 'Companies_with_yud.xlsx')

# /all_cars
def get_all_cars(request):
    query = Car.objects.get_all()
    response = serializers.serialize('json', list(query))
    return HttpResponse(response)


def serve_file(path, filename):
    with open(path, "rb") as excel:
        data = excel.read()
    response = HttpResponse(data, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response


