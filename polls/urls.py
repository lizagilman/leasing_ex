# from django.conf.urls import url
# from . import views
#
# urlpatterns = [
#     # url(r'^$', views.index, name='index'),
#     url(r'^pricelist-for-one-car/(?P<car>\w+)/$', views.get_car_pricelist, name='oneCarPricelist'),
#     url(r'^pricelist-by-company/(?P<company>\w+)/$', views.get_pricelist_by_company, name="pricelistByCompany"),
#     url(r'^active-company-cars/$', views.get_active_companies_cars, name="activeCompanyCars"),
#     url(r'^companies-with-yud/$', views.get_companies_with_yud, name="comapniesWithYud"),
#     url(r'^test/$', views.get_min_price_of_each_company, name="minValues")
# ]

from polls.viewsets.pricelist_viewset import PriceListViewSet
from polls.viewsets.car_viewset import CarViewSet
from polls.viewsets.company_viewset import CompanyViewSet, YudCompanies
from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter
from polls import views


admin.autodiscover()
router = ExtendedDefaultRouter()
(
router.register('pricelist', PriceListViewSet, base_name='pricelist'),
router.register('cars', CarViewSet, base_name='cars'),
router.register('companies', YudCompanies, base_name='company'),
)

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^priceList_by_car/carname=(?P<car_name>.+)/$', views.pricelist_by_car_name, name='priceList_by_car'),
    url(r'^priceList_by_company/company=(?P<company>.+)/$', views.pricelist_by_company, name='priceList_by_company'),
    url(r'^cars_of_active_companies/$', views.get_cars_of_active_companies, name='active_companies'),
    url(r'^download_cars_of_active_companies/$', views.download_cars_of_active_companies, name='active_companies'),
    url(r'^active_companies/$', views.get_active_companies, name='pricelist_for_car'),
    url(r'^yud_companies/$', views.get_companies_contains_yud, name='contains_yud'),
    url(r'^minimum_prices/$', views.get_min_price_of_each_company, name='min_price_list_in_every_company'),
    url(r'^all_cars/$', views.get_all_cars, name='all_cars'),
    url('', include(router.urls)),
]


#
# urlpatterns = [
#     url(),
#
# ]
#     patterns('', url('', include(router.urls)))
#
#