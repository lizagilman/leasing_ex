# -*- coding: utf-8 -*-
from django.test import TestCase
from django.test.client import RequestFactory
from models import Car, Company, PriceList
import pandas as pd
import django
django.setup()


class ModelsTest(TestCase):
    def setUp(self):
        company = Company.objects.create(name="סיקסט"  , city="יהוד", phone_number="050123456", is_active=True)
        car = Car.objects.create(name="toyota", price=124500)
        pricelist = PriceList.objects.create(car=car, company=company, price_at_company=95500)

    def test_get_car_pricelist(self):
        response = self.client.get('/polls/pricelist-for-one-car/toyota/')
        self.assertEqual(response.status_code, 200)

    def test_get_pricelist_by_company(self):
        response = self.client.get('/polls/pricelist-by-company/סיקסט/')
        self.assertEqual(response.status_code, 200)

    def test_get_active_companies_cars(self):
        response = self.client.get('/polls/active-company-cars/')
        self.assertEqual(response.status_code, 200)

    def test_get_companies_with_yud(self):
        response = self.client.get('/polls/companies-with-yud/')
        self.assertEqual(response.status_code, 200)
