from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from polls.models import Car
from polls.serializers.car_serializer import CarSerializer
from polls import querysets


class CarViewSet(NestedViewSetMixin, ModelViewSet):
    serializer_class = CarSerializer
    queryset = Car.objects.all()
