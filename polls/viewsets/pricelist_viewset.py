from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from polls.models import PriceList
from polls.serializers.pricelist_serializer import PriceListSerializer
from polls import querysets


class PriceListViewSet(NestedViewSetMixin, ModelViewSet):
    serializer_class = PriceListSerializer
    queryset = PriceList.objects.all()
