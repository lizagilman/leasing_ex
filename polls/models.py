# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models as baseModel
from model_utils import models
from querysets import PriceListQuerySet, CarQuerySet, CompanyQuerySet
# Create your models here.

CHOICES = (("יהוד", "יהוד"), ("חיפה", "חיפה"), ("ירושלים", "ירושלים"))


class Company(baseModel.Model):
    name = baseModel.CharField(max_length=100)
    city = baseModel.CharField(max_length=50, choices=CHOICES, default=None)
    phone_number = baseModel.CharField(max_length=12, default=None)
    is_active = baseModel.BooleanField(default=False)
    objects = CompanyQuerySet.as_manager()

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def natural_key(self):
        return self.name


class Car(models.TimeStampedModel, baseModel.Model):
    name = baseModel.CharField(max_length=100)
    price = baseModel.FloatField(default=0)
    company = baseModel.ManyToManyField(Company, through='PriceList')
    objects = CarQuerySet.as_manager()

    def __unicode__(self):
        return self.name

    def natural_key(self):
        return self.name


class PriceList(baseModel.Model):
    car = baseModel.ForeignKey(Car)
    company = baseModel.ForeignKey(Company)
    price_at_company = baseModel.FloatField(default=0)
    objects = PriceListQuerySet.as_manager()

    def __unicode__(self):
        return '%s %s' % (self.car.name, self.company.name)
