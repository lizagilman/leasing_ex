from rest_framework import serializers
from polls.models import Company


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('name', 'city', 'phone_number')
