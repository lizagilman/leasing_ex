from rest_framework import serializers
from polls.models import PriceList


class PriceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceList
        fields = ('car', 'company', 'price_at_company')
