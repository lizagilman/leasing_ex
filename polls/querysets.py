# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from model_utils import models
# from django.db.models.query import QuerySet
from django.db import models
from django.db.models import Min
from django.db.models import F


class PriceListQuerySet(models.QuerySet):
    def pricelist_by_car(self, car):
        return self.filter(car__name=car)

    def pricelist_by_company(self, company):
        return self.filter(company__name=company)

    def cars_of_active_companies(self):
        return self.filter(company__is_active=True)

    def min_of_each_company(self):
        # SELECT company, car, price_at_company, min(price_at_company) AS min_price
        # FROM PriceList
        # GROUP BY company
        q = self.annotate(min_price=Min('company__pricelist__price_at_company'))\
            .filter(price_at_company=F('min_price')).only('car', 'company')
        return q


class CarQuerySet(models.QuerySet):
    def car_price(self, car):
        return self.filter(car__name=car)

    def get_all(self):
        return self.all()


class CompanyQuerySet(models.QuerySet):
    def pricelist_of_car(self, car):
        return self.filter(car__name=car)

    def companies_with_yud(self):
        return self.filter(is_active=True).filter(name__contains="י")

    def active_companies(self):
        return self.filter(is_active=True)


